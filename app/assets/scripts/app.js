document.addEventListener("DOMContentLoaded", () => {
	let newMessageForm = document.getElementById('new-message-form');
	newMessageForm.addEventListener('submit', event => {
		event.preventDefault();

		fetch('/message', {
			method: 'post',
			data: {
				message: event.target.message
			}
		}).then(res => {
			console.log(event);
		});
	});
});
