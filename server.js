var express = require('express');
var path = require('path');
var app = express();
var db = require('./server/db.js');

app.use('/assets', express.static(__dirname + '/app/dist'));

app.route('/message')
	.get((req, res) => {
		res.send('Get a random book');
	})
	.post((req, res) => {
		res.send('Posted a random book');
	});

app.get('/', (req, res) => {
	res.sendFile(path.join(__dirname + '/app/index.html'));
});

app.listen(3000, () => {
  console.log('App listening on port 3000!');
});