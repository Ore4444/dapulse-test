var gulp = require("gulp");
var sass = require("gulp-sass");
var babel = require("gulp-babel");

var clientSideJsFiles = "app/assets/scripts/app.js";
var sassFiles = "app/assets/styles/main.scss";

gulp.task("css", function () {
  return gulp.src(sassFiles)
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("app/dist/css"));
});

gulp.task("js", function () {
  return gulp.src(clientSideJsFiles)
    .pipe(babel())
    .pipe(gulp.dest("app/dist/js"));
});

gulp.task("watch", () => {
	gulp.watch(clientSideJsFiles, ['js'])
})

gulp.task("default", ["css", "js"]);